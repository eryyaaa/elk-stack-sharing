#! /bin/bash

cd elasticsearch
docker build -t elasticsearch .

cd ..
cd kibana
docker build -t kibana .

cd ..
cd logstash
docker build -t logstash .

docker pull bitnami/kafka:latest
docker tag bitnami/kafka:latest kafka:latest

docker pull bitnami/zookeeper:latest
docker tag bitnami/zookeeper:latest zookeeper:latest
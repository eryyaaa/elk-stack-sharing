# ELK Stack x Kafka

## ELK Stack
Digunakan sebagai log management. ELK sendiri terdiri dari 3 bagian, yaitu **Elasticsearch**, **Logstash**, dan **Kibana**.

![alt-text](/image/elk.png)

`application.properties`

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-log4j2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.kafka</groupId>
            <artifactId>kafka-log4j-appender</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>org.mongodb</groupId>
            <artifactId>mongo-java-driver</artifactId>
            <version>3.9.0</version>
        </dependency>
```

log4j2.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="info" name="pane-pea-coba" packages="com.emerio.rnd.practice.kafkaappendercoba">
    <Appenders>
        <!-- <Kafka name="Kafka" topic="bali-logs"> -->
        <Kafka name="Kafka" topic="ambon-5k">
            <MarkerFilter marker="bali-access" onMatch="ACCEPT" onMismatch="DENY"/>
            <!-- <JsonLayout/> -->
            <JsonLayout>
                <KeyValuePair key="Hostname Client" value="$${hostName}"/>
                <KeyValuePair key="Access Time" value="$${date:dd/MM/yyyy hh:mm:ss aa}" />
                <KeyValuePair key="Service Name" value="Kafka Appender Coba" />
                <KeyValuePair key="coba user" value="$${env:USER:-jdoe}" />
            </JsonLayout>
            <Property name="targetEnv">${sys:spring.profiles.active}</Property>
            <Property name="bootstrap.servers">kafka:9092</Property>
        </Kafka>
        <Async name="Async">
            <AppenderRef ref="Kafka"/>
        </Async>
        <Console name="stdout" target="SYSTEM_OUT">
            <MarkerFilter marker="bali-access" onMatch="DENY" onMismatch="ACCEPT"/>
            <PatternLayout pattern="%d{dd/MM/yyyy hh:mm:ss aa} %-5p [%-7t] %F:%L - %m%n %p -- %c{1} -- ${serviceName}"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="INFO">
            <AppenderRef ref="Kafka"/>
            <AppenderRef ref="stdout"/>
        </Root>
        <!-- <Logger name="org.apache.kafka" level="WARN" /> -->
        <Logger name="com.emerio.rnd.practice.kafkaappendercoba" level="DEBUG"/>
    </Loggers>
</Configuration>
```

## Logstash
sebuah `Data processing pipeline` , data yang masuk ke Logstash akan di handle sebagai event, yang dapat dipergunakan sesuai kebutuhan. 
Event event yang masuk diproses pada Logstash lalu dikirimkan ke tempat lain, dapat menuju Elasticsearch, Kafka queue, dll.

![alt-text](/image/logstash.png)

Logstash sendiri memiliki 3 stages, **Input Stage**, **Filter Stage**, dan **Output Stage**. Setiap stage memiliki tugas sebagai berikut :

- **Input Stage** adalah saat dimana logstash menerima data dari aplikasi. Input dapat berupa HTTP endpoint, event yang terjadi pada sebuah file, dll.

- **Filter Stage** adalah saat dimana logstash memproses data yang sudah diterima. kita dapat menambahkan filter ataupun kondisi-kondisi, beberapa filter yang sering digunakan adalah `mutate` dan `clone`.

- **Output Stage** adalah saat dimana logstash memberikan output berupa data yang sudah diproses ke `stash`, `stash` dapat berupa database, Elasticsearch, Kafka, dll.

Logstash bersifat `horizontally scalable`, dapat menjalankan banyak pipeline dalam 1 Logstash. 


## Elasticsearch
sebagai `analytical tool` dan `full-text search engine`. Elasticsearch biasa digunakan sebagai tempat penyimpanan dari log yang telah diproses dari logstash. data yang disimpan pada elasticsearch berupa document. Document yang disimpan memiliki bentuk serupa dengan kolom yang biasa digunakan di database seperti di MySQL. Secara tidak langsung document yang digunakan adalah `JSON Object`.
Untuk contoh cara penulisan document sebagai berikut:

```java
Document toDocument(String user, String host, String action, String database, String table, Document payload_before, Document payload_after){
        Document doc = new Document();
        doc.append("user",user);
        doc.append("host", host);
        doc.append("action", action);
        doc.append("database", database);
        doc.append("table", table);
        doc.append("payload_before",payload_before);
        doc.append("payload_after", payload_after);
        return doc;
    }
```

Object document sendiri digunakan agar mempermudah pembuatan JSON formatting pada log message.

## Kibana
sebagai platform untuk visualisasi dan analisis data dari Elasticsearch. dapat diasumsikan sebagai Elasticsearch Data Web Interface. Kibana juga menyediakan interface untuk melakukan `Authentification` dan `Authorization` pada Elasticsearch.

![alt-text](/image/kibana.png)

Kibana pada dasarnya hanya mengirimkan query pada Elasticsearch menggunakan REST API. Kibana menyediakan interface untuk mengirim query query tersebut dan kita dapat mengkonfigurasi bentuk data yang akan ditampilkan. 

## Kafka

Secara singkat, kafka adalah sebuah `streaming platform`.

sebagai sebuah streaming platform, kafka memiliki kemampuan untuk : 

1. dapat menerima (subscribe) stream `record` dan mengirim (publish) straem `record`, mirip dengan `messaging system`.
2. menyimpan stream `record`.
3. mengolah / memproses stream `record`.

![alt-text](/image/kafka_diagram.png)

ada 4 Core API pada Kafka, 2 API yang kita gunakan pada project Bali adalah :

1. `Producer API` untuk membuat service dapat mengirim record dari satu atau banyak Kafka `Topics`
2. `Consumer API` untuk membuat service dapat menerima record dari satu atau banyak Kafka `Topics`


![alt-text](/image/kafka-api.png)

`Topics` adalah kategori atau kelompok dari record yang di publish.

++================================================++

# Installation

Untuk dapat menggunakan ELK pada sebuah aplikasi, perlu dilakukan instalasi. Agar tidak memakan resource yang besar pada device dan memudahkan instalasi, proses instalasi akan dilakukan di docker. Agar bisa terinstall di docker, perlu dibuat konfigurasi dari ELK itu sendiri. 

## Elasticsearch

Pull Elasticsearch Image
- `docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.4`
- cd elasticsearch
- docker build -t elasticsearch

`../elasticsearch/config/elasticsearch.yml`
```sh
---
## Default Elasticsearch configuration from elasticsearch-docker.
## from https://github.com/elastic/elasticsearch-docker/blob/master/build/elasticsearch/elasticsearch.yml
#
cluster.name: "docker-cluster"
network.host: 0.0.0.0

# minimum_master_nodes need to be explicitly set when bound on a public IP
# set to 1 to allow single node clusters
# Details: https://github.com/elastic/elasticsearch/pull/17288
discovery.zen.minimum_master_nodes: 1

## Use single node discovery in order to disable production mode and avoid bootstrap checks
## see https://www.elastic.co/guide/en/elasticsearch/reference/current/bootstrap-checks.html
#
discovery.type: single-node
```

`../elasticsearch/Dockerfile`
```sh
# https://github.com/elastic/elasticsearch-docker
FROM docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.3
COPY ./config/elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml

# Add your elasticsearch plugins setup here
# Example: RUN elasticsearch-plugin install analysis-icu
```

## Logstash

Pull Logstash Image
- `docker pull docker.elastic.co/logstash/logstash:6.2.4`
- cd logstash
- docker build -t logstash

`../logstash/config/logstash.yml`
```sh
---
## Default Logstash configuration from logstash-docker.
## from https://github.com/elastic/logstash-docker/blob/master/build/logstash/config/logstash-oss.yml
#
http.host: "0.0.0.0"
path.config: /usr/share/logstash/pipeline
```

`../logstash/pipeline/logstash.conf`
```yml
input {
    kafka {
        bootstrap_servers => "kafka:9092"
        topics => ["bali-audit-logs","bali-audit-trails","bali-errors"]
	    decorate_events => true
        key_deserializer_class => "org.apache.kafka.common.serialization.StringDeserializer"
        value_deserializer_class => "org.apache.kafka.common.serialization.StringDeserializer"
    }
}

output {
    if [@metadata][kafka][topic] == "bali-audit-logs" {
        stdout {
            codec => rubydebug
        }
        elasticsearch {
            hosts => ["elasticsearch:9200"]
            index => "bali-audit-logs-%{+YYYYMMdd}"
        }
    } 
}
```

`../logstash/Dockerfile`
```sh
# https://github.com/elastic/logstash-docker
FROM docker.elastic.co/logstash/logstash-oss:6.2.3
COPY ./config/logstash.yml /usr/share/logstash/config/logstash.yml
COPY pipeline/* /usr/share/logstash/pipeline/
# RUN rm /usr/share/logstash/pipeline/logstash.conf
#COPY pipeline/logstash-bali-audit-logs.conf /usr/share/logstash/pipeline/logstash.conf
# Add your logstash plugins setup here
# Example: RUN logstash-plugin install logstash-filter-json
```

## Kibana

Pull Kibana Image
- `docker pull docker.elastic.co/kibana/kibana:6.2.4`
- cd kibana
- docker build -t kibana

`../kibana/config/kibana.yml`
```yml
---
## Default Kibana configuration from kibana-docker.
## from https://github.com/elastic/kibana-docker/blob/master/build/kibana/config/kibana.yml
#
server.name: kibana
server.host: "0"
elasticsearch.url: http://elasticsearch:9200
# elasticsearch.hosts: http://elasticsearch:9200
```

`../kibana/Dockerfile`
```sh
# https://github.com/elastic/kibana-docker
FROM docker.elastic.co/kibana/kibana-oss:6.2.3
COPY ./config/kibana.yml /usr/share/kibana/config/kibana.yml
# Add your kibana plugins setup here
# Example: RUN kibana-plugin install <name|url>
RUN kibana-plugin install https://github.com/sirensolutions/sentinl/releases/download/tag-6.2.4-3-public/sentinl-v6.2.3.zip
# tes
```

## Kafka + Zookeeper

Pull Zookeeper Image
- `docker pull zookeeper`
- `docker tag bitnami/kafka:latest zookeeper:latest`

Pull Kafka Image
- `docker pull bitnami/kafka`
- `docker tag bitnami/kafka:latest kafka:latest`

Untuk kafka sendiri, konfigurasi dilakukan dalam docker compose.

```yml
zookeeper:
    image: zookeeper:latest
    networks: 
      - backend
    ports:
      - "2181:2181"
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
    restart: unless-stopped

  kafka:
    image: kafka:latest
    networks: 
      - backend
    depends_on: 
      - zookeeper
    links: 
      - zookeeper
    ports:
      - "9092:9092"
    environment:
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181
      - ALLOW_PLAINTEXT_LISTENER=yes
      - KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://kafka:9092
    restart: unless-stopped
```



Image yang sudah dibuat perlu di run, untuk run semua image dapat menggunakan **Docker Compose**

`docker-compose-elk.yml`
```yml
version: "3"

networks:
  backend:
  
volumes:
  elasticsearch:
  
services:

# START INFRASTRUCTURE
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.2.4
    ports: 
      - "9200:9200"
      - "9300:9300"
    networks:
      - backend
    volumes:
      - elasticsearch:/usr/share/elasticsearch/data 
    environment:
      ES_JAVA_OPTS: "-Xmx256m -Xms256m"
    restart: unless-stopped

  logstash:
    image: docker.elastic.co/logstash/logstash:6.2.4
    ports:
      - "5043:5043"
    links:
      - elasticsearch
      - kafka
    networks: 
      - backend
    depends_on:
      - elasticsearch
    volumes:
       - /home/adhiguna/Documents/elk-stack/logstash/pipeline/:/usr/share/logstash/pipeline
    environment:
      JS_JAVA_OPTS: "-Xmx256m -Xms256m"
    restart: unless-stopped

  kibana:
    image: docker.elastic.co/kibana/kibana:6.2.4
    ports: 
      - "5601:5601"
    links: 
      - elasticsearch
    depends_on: 
      - elasticsearch
    networks: 
      - "backend"
    restart: unless-stopped

  zookeeper:
    image: zookeeper:latest
    networks: 
      - backend
    ports:
      - "2181:2181"
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
    restart: unless-stopped

  kafka:
    image: kafka:latest
    networks: 
      - backend
    depends_on: 
      - zookeeper
    links: 
      - zookeeper
    ports:
      - "9092:9092"
    environment:
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181
      - ALLOW_PLAINTEXT_LISTENER=yes
      - KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://kafka:9092
    restart: unless-stopped
  # END INFRASTRUCTURE
```

Seperti yang dilihat pada configurasi diatas, masing-masing service diberikan port tersendiri yang berbeda-beda. Fungsinya menggunakan docker compose pada saat menjalankan semua service adalah agar setiap service yang akan digunakan dapat berjalan bersamaan.